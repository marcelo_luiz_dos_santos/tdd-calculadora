package br.com.calculadora;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CalculadoraTeste {

    private Calculadora calculadora;

    @BeforeEach
    public void setUp(){
        calculadora = new Calculadora();
    }

    @Test
    public void testaASomaDeDoisNumerosInteiros(){
        int resultado = calculadora.soma(1, 2);

        Assertions.assertEquals(3, resultado);
    }

    @Test
    public void testaASomaDeDoisNumerosFlutuantes(){
        double resultado = calculadora.soma(2.3, 3.4);

        Assertions.assertEquals(5.7, resultado);
    }

    @Test
    public void testaADivisaoPorNumerosInteiros(){
        int quociente = calculadora.divisao(10, 2);

        Assertions.assertEquals(5, quociente);
    }

    @Test
    public void testaADivisaoDeNumerosnegativos(){
        int quociente = calculadora.divisao(-10, 2);

        Assertions.assertEquals(-5, quociente);
    }

    @Test
    public void testaADivisaoDeNumerosflutuantes(){
        double quociente = calculadora.divisao(10.5, 2);

        Assertions.assertEquals(5.25, quociente);
    }

    @Test
    public void testaAMultiplicacaoPorNumerosInteiros(){
        int produto = calculadora.multiplicacao(10, 2);

        Assertions.assertEquals(20, produto);
    }

    @Test
    public void testaAMultiplicacaoDeNumerosnegativos(){
        int produto = calculadora.multiplicacao(-10, -2);

        Assertions.assertEquals(20, produto);
    }

    @Test
    public void testaAMultiplicacaoDeNumerosflutuantes(){
        double produto = calculadora.multiplicacao(10.75, 2);

        Assertions.assertEquals(21.5, produto);
    }

    @Test
    public void testaCaminhoTristeDaSoma(){
        Assertions.assertThrows(RuntimeException.class, () -> {calculadora.soma(-1, 0);});
    }
}
