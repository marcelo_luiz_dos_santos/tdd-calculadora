package br.com.calculadora;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Calculadora {

    public Calculadora(){}

    public int soma(int primeiroNumero, int segundoNumero){
        if(primeiroNumero < 0 || segundoNumero <0) {
            throw new RuntimeException("Os números não podem ser negativos");
        }

        int resultado = primeiroNumero + segundoNumero;
        return resultado;
    }

    public double soma(double primeiroNumero, double segundoNumero){
        Double resultado = primeiroNumero + segundoNumero;
        BigDecimal bigDecimal = new BigDecimal(resultado).setScale(3, RoundingMode.HALF_EVEN);

        return bigDecimal.doubleValue();
    }

    public int divisao(int dividendo, int divisor){
        int quociente = dividendo / divisor;
        return  quociente;
    }

    public double divisao(double dividendo, double divisor){
        double quociente = dividendo / divisor;
        return  quociente;
    }

    public int multiplicacao(int coeficiente, int multiplicador){
        int produto = coeficiente * multiplicador;
        return produto;
    }

    public double multiplicacao(double coeficiente, double multiplicador){
        double produto = coeficiente * multiplicador;
        return produto;
    }
}
